<?php

namespace Lmn\Account\Exception;

class AccessDeniedException extends \Exception {

    public function __construct($message,$code = 401, $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

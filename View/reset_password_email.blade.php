<!DOCTYPE html>
<html>
  <head>
        <link rel="stylesheet" type="text/css" href="app/assets/css/email.css">

        <meta charset="UTF-8">
    </head>
    <body>
        <div>
            <h1>Ahoj {{$name}},</h1>
            <p>Tiež máme občas problém s pamataním hesiel. Nič si z toho nerob, posielame ti nové. Je pomerne bezpečné, no aj tak ho odporúčame po prihlásení hneď zmeniť. Tvoje nové heslo je: <br />{{$password}}</p>
            <p>Vidíme sa opäť na Veski!</p>
            <p>Tím Veski</p>
        </div>
    </body>
</html>
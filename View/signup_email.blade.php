<!DOCTYPE html>
<html>
  <head>
        <link rel="stylesheet" type="text/css" href="app/assets/css/email.css">

        <meta charset="UTF-8">
    </head>
    <body>
        <div>
            <h1>Ahoj {{$name}},</h1>
            <p>Vitaj na stránke Veski! Už o chvíľu si môžeš užívať našu stránku vytvorenú študentami, pre študentov. Pred prvým prihlásením však musíš potvrdiť registráciu cez nasledujúci link: <br><a href="http://www.veski.sk/validate?email={{$email}}&token={{$token}}">{{$token}}</a></p>
            <p>Tešíme sa na teba!</p>
            <p>Tím Veski</p>
            <p>Ak ti tento email prišiel iba náhodou, prosím ignoruj ho.</p>
        </div>
    </body>
</html>
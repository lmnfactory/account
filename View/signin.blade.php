<html>
    <head>
        <title>Google sign in</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--    <link rel="stylesheet" href="styles.css">-->
        <script src="assets/vendor/lmn.core/core-dep.js"></script>
        <script src="https://apis.google.com/js/api.js"></script>
    </head>
    <body>
        <script>
          window.fbAsyncInit = function() {
            FB.init({
              appId      : '1890653467831149',
              xfbml      : true,
              version    : 'v2.8'
            });
            FB.AppEvents.logPageView();
          };

          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "//connect.facebook.net/sk_SK/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
        </script>

            <div id="root"></div>
            <script src="assets/vendor/lmn.factory/factory.js"></script>
            <script src="assets/vendor/lmn.core/core.js"></script>
            <script src="assets/vendor/lmn.account/account.js"></script>
            <script src="assets/vendor/lmn.subject/subject.js"></script>
            <script src="assets/vendor/lmn.thread/thread.js"></script>
            <script src="assets/vendor/lmn.calendar/calendar.js"></script>

            <script src="assets/vendor/lmn.app.veski/app.js"></script>
    </body>
</html>

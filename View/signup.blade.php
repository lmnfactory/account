<html>
    <head>
        <title>Google sign in</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--    <link rel="stylesheet" href="styles.css">-->
        <script src="assets/vendor/lmn.core/core-dep.js"></script>
        <script src="https://apis.google.com/js/api.js"></script>
    </head>
    <body>
        <div id="root"></div>

        <script src="assets/vendor/lmn.factory/factory.js"></script>
        <script src="assets/vendor/lmn.core/core.js"></script>
        <script src="assets/vendor/lmn.account/account.js"></script>

        <script src="assets/vendor/lmn.app.faceschool/app.js"></script>
    </body>
</html>

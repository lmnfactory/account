<?php

namespace Lmn\Account;

use Lmn\Core\Provider\LmnServiceProvider;
use Lmn\Core\Lib\Module\ModuleServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

use Lmn\Core\Lib\Repository\Config\RepositoryConfig;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Database\GeneratorService;
use Lmn\Core\Lib\Instance\InstanceService;
use Lmn\Core\Lib\Map\MapService;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Lib\Response\ResponseEvent;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\JWT\JWTService;
use Lmn\Core\Lib\Exception\ExceptionService;
use Lmn\Core\Lib\Database\Seeder\SeederService;

use Lmn\Account\Command\LmnAuthConfigCommand;

use Lmn\Account\Middleware\SigninRequiredMiddleware;

use Lmn\Account\Database\Seed\UserSeeder;

use Lmn\Account\Repository\UserRepository;
use Lmn\Account\Repository\UserPasswordRepository;
use Lmn\Account\Repository\AuthRepository;
use Lmn\Account\Repository\Config\UserConfig;
use Lmn\Account\Repository\Criteria\User\UserDefaultCriteria;
use Lmn\Account\Repository\Criteria\User\UserByLoginCriteria;
use Lmn\Account\Repository\Criteria\User\UserByEmailCriteria;
use Lmn\Account\Repository\Criteria\User\UserValidateCriteria;
use Lmn\Account\Repository\Criteria\User\UserBySeriesCriteria;
use Lmn\Account\Repository\Criteria\User\UserByPrivateIdCriteria;
use Lmn\Account\Repository\Criteria\User\UserByPublicIdCriteria;
use Lmn\Account\Repository\Criteria\User\UserExtensionIdCriteria;
use Lmn\Account\Repository\Criteria\User\UserExtensionIdsCriteria;
use Lmn\Account\Repository\Criteria\Remember\RememberBySeriesCriteria;
use Lmn\Account\Repository\Criteria\Auth\AuthorizeCriteria;
use Lmn\Account\Repository\UserPublicRepository;
use Lmn\Subject\Repository\Criteria\SubjectByPublicIdCriteria;
use Lmn\Account\Repository\RememberRepository;
use Lmn\Account\Repository\RefreshtokenRepository;

use Lmn\Account\Lib\User\UserService;

use Lmn\Account\Lib\Google\GoogleAuthService;
use Lmn\Account\Lib\Google\GoogleAuthValidation;
use Lmn\Account\Lib\Facebook\FacebookAuthService;
use Lmn\Account\Lib\Facebook\FacebookAuthValidation;
use Lmn\Account\Lib\Lmn\LmnAuthService;
use Lmn\Account\Lib\Lmn\LmnAuthValidation;

use Lmn\Account\Lib\Auth\AuthService;
use Lmn\Account\Lib\Auth\AuthResponseListener;
use Lmn\Account\Lib\Auth\UnauthorizedException;
use Lmn\Account\Lib\Auth\CurrentUser;

use Lmn\Account\Lib\Auth\Token\TokenService;
use Lmn\Account\Lib\Auth\Token\JWTAuthToken;
use Lmn\Account\Lib\Auth\Token\JWTRefreshToken;
use Lmn\Account\Lib\Auth\Token\JWToken;
use Lmn\Account\Lib\Auth\Validation\RefreshTokenValidation;
use Lmn\Account\Lib\Auth\AuthorizationFailedException;
use Lmn\Account\Lib\Remember\Token\RememberToken;

use Lmn\Account\Lib\Remember\RememberService;
use Lmn\Account\Lib\Remember\RememberTokenValidation;

use Lmn\Account\Lib\Exception\UnauthorizedExceptionHandler;
use Lmn\Account\Lib\Exception\AccessDeniedExceptionHandler;
use Lmn\Account\Lib\Exception\AuthorizationFailedExceptionHandler;

use Lmn\Account\Database\Validation\UserValidation;
use Lmn\Account\Database\Validation\AuthRequestValidation;
use Lmn\Account\Database\Validation\SignupFormValidation;
use Lmn\Account\Database\Validation\SigninFormValidation;
use Lmn\Account\Database\Validation\ValidateFormValidation;
use Lmn\Account\Database\Validation\UserValidateValidation;
use Lmn\Account\Database\Validation\ResetPasswordFormValidation;
use Lmn\Account\Database\Validation\ChangePasswordFormValidation;
use Lmn\Account\Database\Validation\UserProfileUpdateFormValidation;
use Lmn\Account\Exception\AccessDeniedException;

class ModuleProvider implements ModuleServiceProvider{

    public function register(LmnServiceProvider $provider) {
        $app = $provider->getApp();

        // TokenService
        $app->singleton(TokenService::class, function($app) {
            $mapService = $app->make('mapService');
            return new TokenService($app->make(InstanceService::class), $mapService->make('oneToOne'));
        });
        $app->singleton(AuthService::class, AuthService::class);
        $app->singleton(RememberService::class, RememberService::class);
        $app->singleton(UserService::class, UserService::class);
        $app->singleton(CurrentUser::class, CurrentUser::class);

        $app->singleton(UserConfig::class, UserConfig::class);

        $app->singleton(UserRepository::class, UserRepository::class);
        $app->singleton(UserPasswordRepository::class, UserPasswordRepository::class);
        $app->singleton(UserPublicRepository::class, function($app) {
            return new UserPublicRepository($app->make(CriteriaService::class), $app->make(UserConfig::class));
        });
        $app->singleton(AuthRepository::class, function($app) {
            return new AuthRepository($app->make(CriteriaService::class));
        });

        $app->singleton(RefreshtokenRepository::class, function($app) {
            return new RefreshtokenRepository($app->make(CriteriaService::class));
        });

        $app->singleton(RememberRepository::class, function($app) {
            return new RememberRepository($app->make(CriteriaService::class));
        });

        $this->registerCommands($provider);
    }

    public function boot(LmnServiceProvider $provider) {
        $app = $provider->getApp();
        /** @var SeederService */
        $seederService = $app->make(SeederService::class);
        $seederService->addSeeder(UserSeeder::class);

        /** @var ResponseService $responseService */
        $responseService = $app->make(ResponseService::class);
        $signinRequiredMessage = $responseService->createMessage("User has to be signed in", 401);
        $responseService->prepare('signin.required', $signinRequiredMessage);

        /** @var AuthService */
        $authService = $provider->getApp()->make(AuthService::class);
        $authService->addMethod(GoogleAuthService::class);
        $authService->addMethod(FacebookAuthService::class);
        $authService->addMethod(LmnAuthService::class);

        /** @var JWTService $jwtService */
        $jwtService = \App::make(JWTService::class);
        $jwtService->setSecret(config('module.auth.secret', false));

        /** @var TokenService $tokenService */
        $tokenService = \App::make(TokenService::class);
        $tokenService->add('jwt', function() use($jwtService) {
            return new JWToken($jwtService);
        });
        $tokenService->add('jwt.auth', function() use($jwtService) {
            return new JWTAuthToken($jwtService);
        });
        $tokenService->add('jwt.refresh', function() use($jwtService) {
            return new JWTRefreshToken($jwtService);
        });
        $tokenService->add('remember', function() {
            return new RememberToken();
        });

        /** @var ValidationService */
        $validationService = $provider->getApp()->make(ValidationService::class);
        $validationService->add('user', UserValidation::class);
        $validationService->add('user.validate', UserValidateValidation::class);
        $validationService->add('form.signup', SignupFormValidation::class);
        $validationService->add('form.signin', SigninFormValidation::class);
        $validationService->add('form.validate', ValidateFormValidation::class);
        $validationService->add('form.resetpassword', ResetPasswordFormValidation::class);
        $validationService->add('form.changepassword', ChangePasswordFormValidation::class);
        $validationService->add('auth.request', AuthRequestValidation::class);
        $validationService->add('auth.google', GoogleAuthValidation::class);
        $validationService->add('auth.facebook', FacebookAuthValidation::class);
        $validationService->add('auth.lmn', LmnAuthValidation::class);
        $validationService->add('token.remember', RememberTokenValidation::class);
        $validationService->add('refreshtoken.refresh', RefreshTokenValidation::class);
        $validationService->add('user.profile.update', UserProfileUpdateFormValidation::class);

        \Validator::extend('passwordCheck', '\\Lmn\\Account\\Lib\\Model\\ValidationRule\\PasswordCheck@rule');
        \Validator::extend('validUser', '\\Lmn\\Account\\Lib\\Model\\ValidationRule\\ValidUser@rule');
        \Validator::extend('validationToken', '\\Lmn\\Account\\Lib\\Model\\ValidationRule\\ValidationToken@rule');

        /** @var CriteriaService */
        $criteriaService = $app->make(CriteriaService::class);
        $criteriaService->add('user.default', UserDefaultCriteria::class);
        $criteriaService->add('user.validate', UserValidateCriteria::class);
        $criteriaService->add('user.by.privateId', UserByPrivateIdCriteria::class);
        $criteriaService->add('user.by.publicId', UserByPublicIdCriteria::class);
        $criteriaService->add('user.by.login', UserByLoginCriteria::class);
        $criteriaService->add('user.by.email', UserByEmailCriteria::class);
        $criteriaService->add('user.by.series', UserBySeriesCriteria::class);
        $criteriaService->add('user.ext.id', UserExtensionIdCriteria::class);
        $criteriaService->add('user.ext.ids', UserExtensionIdsCriteria::class);
        $criteriaService->add('remember.by.series', RememberBySeriesCriteria::class);
        $criteriaService->add('auth.authorize', AuthorizeCriteria::class);

        /** @var ExceptionService */
        $exceptionService = $provider->getApp()->make(ExceptionService::class);
        $exceptionService->add(UnauthorizedException::class, new UnauthorizedExceptionHandler());
        $exceptionService->add(AuthorizationFailedException::class, new AuthorizationFailedExceptionHandler());
        $exceptionService->add(AccessDeniedException::class, new AccessDeniedExceptionHandler());

    }

    public function event(LmnServiceProvider $provider) {

    }

    public function route(LmnServiceProvider $provider) {
        Route::group(['namespace' => 'Lmn\\Account\\Controller'], function() {
            Route::any('signin','AuthController@index');
            
            Route::post('auth/signup','AuthController@signup');
            Route::post('auth/signin','AuthController@signin');
            Route::any('auth/signout','AuthController@signout')->middleware(SigninRequiredMiddleware::class);
            Route::post('auth/refresh','AuthController@refresh');
            //Route::any('auth/longtoken','AuthController@longtoken');
            Route::any('auth/autosignin','AuthController@autosignin');
            Route::any('auth/validate','AuthController@validateUser');

            Route::any('api/user/profile/update','UserController@update')->middleware(SigninRequiredMiddleware::class);
            Route::any('api/user/profile','UserController@getDetail')->middleware(SigninRequiredMiddleware::class);
            Route::any('user/reset_password','UserController@resetPassword');
            Route::any('user/change_password','UserController@changePassword')->middleware(SigninRequiredMiddleware::class);

            Route::any('test/auth/authAction','TestController@authAction')->middleware(SigninRequiredMiddleware::class);
            Route::any('test/auth/unauthAction','TestController@unauthAction');
            //Route::any('auth/google/activities','GoogleAuthentificationController@activities');
            //Route::any('auth/google/connect/{state}','GoogleAuthentificationController@connect');
        });
    }

    public function registerCommands($provider){
        $app = $provider->getApp();
        $app['lmn.auth.config'] = $provider->getApp()->share(function () {
            return new LmnAuthConfigCommand();
        });

        $provider->commands('lmn.auth.config');
    }
}

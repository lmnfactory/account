<?php

namespace Lmn\Account\Middleware;

use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Account\Lib\Auth\UnauthorizedException;
use Lmn\Account\Lib\Auth\AuthService;

class SigninRequiredMiddleware {

    /** @var AuthService */
    private $authService;
    /** @var ResponseService */
    private $responseService;

    public function __construct(AuthService $authService, ResponseService $responseService) {
        $this->authService = $authService;
        $this->responseService = $responseService;
    }

    public function handle($request, \Closure $next) {
        if (!$this->authService->isSignedIn())  {
            return $this->responseService->use('signin.required');
        }

        return $next($request);
    }
}

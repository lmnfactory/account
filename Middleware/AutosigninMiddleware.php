<?php

namespace Lmn\Account\Middleware;

use Illuminate\Http\Request;
use Lmn\Account\Lib\Auth\AuthService;

class AutosigninMiddleware {

    private $authService;

    public function __construct(AuthService $authService) {
        $this->authService = $authService;
    }

    public function handle(Request $request, \Closure $next) {
        $token = $request->header('JWTAuth', null);
        $this->authService->autoSignin($token);

        return $next($request);
    }
}

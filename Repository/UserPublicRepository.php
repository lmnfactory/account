<?php

namespace Lmn\Account\Repository;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Config\RepositoryConfig;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\Core\Lib\Database\GeneratorService;
use Lmn\Core\Lib\Facade\Generator;
use Lmn\Account\Database\Model\User;

/**
 * @property GeneratorService $generatorService
 */
class UserPublicRepository extends AbstractEloquentRepository {

    private $config;

    public function __construct(CriteriaService $criteriaService, RepositoryConfig $config) {
        parent::__construct($criteriaService);
        $this->config = $config;
    }

    public function getModel() {
        return User::class;
    }

    public function get() {
        $user = parent::get();

        $extensions = $this->config->get('extensions');
        if (is_array($extensions)) {
            foreach ($extensions as $key => $ext) {
                $user->$key = $ext->clear()
                    ->criteria('user.ext.id', ['userId' => $user->id])
                    ->criteria('user.select.public')
                    ->get();
            }
        }

        return $user;
    }
}

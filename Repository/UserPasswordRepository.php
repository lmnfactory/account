<?php

namespace Lmn\Account\Repository;

use Lmn\Core\Lib\Repository\AbstractEventEloquentRepository;
use Lmn\Core\Lib\Repository\Config\RepositoryConfig;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\Core\Lib\Database\GeneratorService;
use Lmn\Core\Lib\Facade\Generator;
use Lmn\Account\Database\Model\User;

/**
 * @property GeneratorService $generatorService
 */
class UserPasswordRepository extends AbstractEventEloquentRepository {

    private $generatorService;

    public function __construct(CriteriaService $criteriaService, GeneratorService $generatorService) {
        parent::__construct($criteriaService);
        $this->generatorService = $generatorService;
    }

    public function getModel() {
        return User::class;
    }

    public function update($data) {
        $user = $this->get();
        if (!$user) {
            return;
        }

        if (isset($data['password'])){
            $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
        }

        $user->save();
    }
}

<?php

namespace Lmn\Account\Repository;

use Lmn\Core\Lib\Repository\AbstractEventEloquentRepository;
use Lmn\Core\Lib\Repository\Config\RepositoryConfig;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Core\Lib\Database\Save\SaveService;
use Lmn\Core\Lib\Database\GeneratorService;
use Lmn\Core\Lib\Facade\Generator;
use Lmn\Account\Database\Model\User;

/**
 * @property GeneratorService $generatorService
 */
class UserRepository extends AbstractEventEloquentRepository {

    private $generatorService;

    public function __construct(CriteriaService $criteriaService, GeneratorService $generatorService) {
        parent::__construct($criteriaService);
        $this->generatorService = $generatorService;
    }

    public function getModel() {
        return User::class;
    }

    public function create($data) {
        $model = $this->getModel();

        if (!isset($data['fullname'])) {
            $data['fullname'] = $data['name']." ".$data['surname'];
        }

        $user = new $model();
        $user->fill($data);

        $user->active = true;
        $user->public_id = $this->generatorService->uniqueString('user', 'public_id');
        $user->private_id = $this->generatorService->uniqueString('user', 'private_id');
        $user->series = $this->generatorService->uniqueString('user', 'series');

        if (isset($data['password'])){
            $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
            $user->validationtoken = $this->generatorService->uniqueString('user', 'validationtoken');
        }

        //TODO transaction
        $user->save();

        $this->onCreate($user, $data);

        return $user;
    }

    public function update($data) {
        $model = $this->get();
        if (isset($data['series'])) {
            if (!$this->generatorService->isUniqueString('user', 'series', $data['series'])) {
                $data['series'] = $this->generatorService->uniqueString('user', 'series');
            }
        }

        if (isset($data['name']) || isset($data['surname'])) {
            $fullname = "";
            if (isset($data['name'])) {
                $fullname = $data['name'];
            } else {
                $fullname = $model->name;
            }

            if (isset($data['surname'])) {
                $fullname .= " " . $data['surname'];
            } else {
                $fullname .= " " . $model->surname;
            }
            $data['fullname'] = $fullname;
        }

        return parent::update($data);
    }
}

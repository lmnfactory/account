<?php

namespace Lmn\Account\Repository\Criteria\Remember;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class RememberBySeriesCriteria implements Criteria {

    private $series;

    public function __construct() {

    }

    public function set($args) {
        $this->series = $args['series'];
    }

    public function apply(Builder $query) {
        $query->where('series', '=', $this->series);
    }
}

<?php

namespace Lmn\Account\Repository\Criteria\Auth;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class AuthorizeCriteria implements Criteria {

    private $method;
    private $uid;

    public function __construct() {

    }

    public function set($args = []) {
        $this->method = $args['method'];
        $this->uid = $args['uid'];
    }

    public function apply(Builder $query) {
        $query->where('method', '=', $this->method)
            ->where('uid', '=', $this->uid);
    }
}

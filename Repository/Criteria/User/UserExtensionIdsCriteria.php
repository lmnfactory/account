<?php

namespace Lmn\Account\Repository\Criteria\User;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class UserExtensionIdsCriteria implements Criteria {

    private $userIds;

    public function __construct() {

    }

    public function set($args) {
        $this->userIds = $args['userIds'];
    }

    public function apply(Builder $builder) {
        $builder->whereIn('user_id', $this->userIds);
    }
}

<?php

namespace Lmn\Account\Repository\Criteria\User;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class UserValidateCriteria implements Criteria {

    private $email;
    private $token;

    public function __construct() {

    }

    public function set($args) {
        $this->email = $args['email'];
        $this->token = $args['token'];
    }

    public function apply(Builder $query) {
        $query->where('email', '=', $this->email)
                ->where('validationtoken', '=', $this->token)
                ->where('valid', '=', false);
    }
}

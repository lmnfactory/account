<?php

namespace Lmn\Account\Repository\Criteria\User;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class UserDefaultCriteria implements Criteria {

    public function __construct() {

    }

    public function set($args) {

    }

    public function apply(Builder $query) {
        $query->where('valid', '=', true)
            ->where('active', '=', true);
    }
}

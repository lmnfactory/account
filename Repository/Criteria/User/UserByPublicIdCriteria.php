<?php

namespace Lmn\Account\Repository\Criteria\User;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class UserByPublicIdCriteria implements Criteria {

    private $publicId;

    public function __construct() {

    }

    public function set($args) {
        $this->publicId = $args['publicId'];
    }

    public function apply(Builder $query) {
        $query->where('public_id', '=', $this->publicId);
    }
}

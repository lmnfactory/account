<?php

namespace Lmn\Account\Repository\Criteria\User;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class UserByLoginCriteria implements Criteria {

    private $email;

    public function __construct() {

    }

    public function set($args) {
        $this->email = $args['email'];
    }

    public function apply(Builder $query) {
        $query->where('email', '=', $this->email)
                ->whereNotNull('password');
    }
}

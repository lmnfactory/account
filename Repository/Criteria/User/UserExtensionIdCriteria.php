<?php

namespace Lmn\Account\Repository\Criteria\User;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class UserExtensionIdCriteria implements Criteria {

    private $userId;

    public function __construct() {

    }

    public function set($args) {
        $this->userId = $args['userId'];
    }

    public function apply(Builder $builder) {
        $builder->where('user_id', '=', $this->userId);
    }
}

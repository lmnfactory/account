<?php

namespace Lmn\Account\Repository\Criteria\User;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class UserByPrivateIdCriteria implements Criteria {

    private $privateId;

    public function __construct() {

    }

    public function set($args) {
        $this->privateId = $args['privateId'];
    }

    public function apply(Builder $query) {
        $query->where('private_id', '=', $this->privateId);
    }
}

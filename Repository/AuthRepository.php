<?php

namespace Lmn\Account\Repository;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Account\Database\Model\Auth;

class AuthRepository extends AbstractEloquentRepository {

    public function __construct(CriteriaService $criteriaService) {
        parent::__construct($criteriaService);
    }

    public function getModel() {
        return Auth::class;
    }
}

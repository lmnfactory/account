<?php

namespace Lmn\Account\Repository\Config;

use Lmn\Core\Lib\Repository\Config\AbstractRepositoryConfig;

class UserConfig extends AbstractRepositoryConfig {

    public function __construct() {
        parent::__construct();
    }
}

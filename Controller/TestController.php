<?php

namespace Lmn\Account\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Response\ResponseService;

class TestController extends Controller {

    public function __construct() {

    }

    public function unauthAction(Request $request, ResponseService $responseService){
        return $responseService->response("Alright alright alright.");
    }

    public function authAction(Request $request, ResponseService $responseService){
        return $responseService->response("Alright alright alright.");
    }
}

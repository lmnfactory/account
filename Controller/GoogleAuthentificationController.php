<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Lmn\Account\Controller;

/**
 * Description of GoogleAuthorizationController
 *
 * @author arksys
 */

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Controllers\Controller;
use Lmn\Core\Lib\Facade\Config;

use Google_Client;
use Google_Service_Plus;
use Lmn\Account\Lib\Google\GoogleAuthService;

class GoogleAuthentificationController extends Controller{

    private $client_id;
    private $client_secret;
    private $state;

    private $client;
    private $plus;

    public function __construct() {
        $googleConfig = Config::get("lmn.account.access.google", false);
        //
        if ($googleConfig === false){
            echo "Google config not specified.";
        }

        $this->client_id = $googleConfig['client_id'];
        $this->client_secret = $googleConfig['client_secret'];
        session(['state' => "AAA"]);
        //
        // $this->client = new Google_Client();
        // $this->client->setApplicationName("NAME");
        // $this->client->setClientId($this->client_id);
        // $this->client->setClientSecret($this->client_secret);
        // $this->client->setRedirectUri('auth/google/redirect');
        // $this->plus = new Google_Service_Plus($this->client);
    }


    public function auth(){
        return view("lmn.account::gauth", [
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
            'STATE' => session('state')
        ]);
    }

    public function index(){
        return view("lmn.account::googlesignin", [
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
            'STATE' => session('state')
        ]);
    }

    public function signin(Request $request, GoogleAuthService $googleAuthService){
        $data = $request->json('data');

        $googleUser = $data['googleUser'];
        $googleData = $googleAuthService->signin($googleUser);

        return $googleData;
    }

    public function activities(Request $request){

    }

    public function connect(Request $request, $state){

    }
}

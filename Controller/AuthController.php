<?php

namespace Lmn\Account\Controller;

use Lmn\Core\Lib\Facade\Config;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Lib\Response\ResponseMessage;
use Lmn\Core\Lib\JWT\JWTService;
use Lmn\Account\Lib\Auth\AuthService;
use Lmn\Account\Lib\User\UserService;
use Lmn\Account\Lib\Auth\Token\TokenService;
use Lmn\Account\Lib\Remember\RememberService;
use Lmn\Core\Lib\Model\ValidationException;
use Lmn\Core\Lib\Model\ValidationService;

/**
 * @property ResponeService $responseService
 */
class AuthController extends Controller {

    /** @var ResponseService $responseService */
    private $responseService;

    private $client_id;
    private $client_secret;

    public function __construct(ResponseService $responseService) {
        $this->responseService = $responseService;
        $googleConfig = Config::get("lmn.account.access.google", false);
        if ($googleConfig === false){
            echo "Google config not specified.";
        }

        $this->client_id = $googleConfig['client_id'];
        $this->client_secret = $googleConfig['client_secret'];
    }

    private function tokensToMessage($tokens, ResponseMessage $message = null) {
        if ($message == null) {
            $message = $this->responseService->createMessage("");
        }

        foreach ($tokens as $key => $token) {
            $message->setHeader($key, $token->encode());
        }

        return $message;
    }

    public function index(){
        return view("lmn.account::signin", [
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
        ]);
    }

    /**
     * Force doing autosign in. Autosign in is a process of recognizing user by JWTAuth token or remember me token (RMAuth). This recognition is done via middleware, but can be forced by this controller method.
     * @method autosignin
     * @param  Request         $reqeust
     * @param  ResponseService $responseService
     * @return array
     */
    public function autosignin(Request $request, ResponseService $responseService, AuthService $authService){
        $RMAuthHeader = $request->header("RMAuth", null);

        $user = $authService->rememberSignin($RMAuthHeader);
        $tokens = $authService->getAuthTokens($user);

        $message = null;
        if ($user != null) {
            $message = $responseService->createMessage($user->toArray());
        }
        $message = $this->tokensToMessage($tokens, $message);
        $message->addOption('expires_in', config('module.auth.jwtauth.exp'));

        return $responseService->send($message);
    }

    /**
     * Sign up.
     * @method signup
     * @param  Request           $request
     * @param  UserService       $userService
     * @param  ResponseService   $responseService
     * @param  ValidationService $validationService
     * @return array
     */
    public function signup(Request $request, ResponseService $responseService, UserService $userService, ValidationService $validationService){
        $data = $request->json()->all();

        // validte user input (form)
        if (!$validationService->validate($data, 'form.signup')) {
            return $responseService->response($validationService->getErrorsArray(), 422);
        }

        $user = $userService->register($data);

        $emailData = [
            'name' => $user->name,
            'email' => urlencode($user->email),
            'token' => urlencode($user->validationtoken)
        ];
        Mail::send('lmn.account::signup_email', $emailData, function($m) use ($user) {
            $m->from('noreply@veski.sk', 'Veski');
            $m->to($user->email, $user->name)->subject('Vitaj na Veski! Prosím potvrď svoju registráciu');
        });

        return $responseService->response($user->mask());
    }

    /**
     * Sign in
     * @method signin
     * @param  Request         $request
     * @param  AuthService     $authService
     * @param  ResponseService $responseService
     * @return array
     */
    public function signin(Request $request, AuthService $authService, RememberService $rememberService, ResponseService $responseService, TokenService $tokenService, ValidationService $validationService){
        $data = $request->json()->all();

        // try to sign in user. You don't know what method user selected.
        try {
            $user = $authService->signin($data);
        } catch (ValidationException $ex) {
            return $responseService->response($validationService->getErrorsArray(), 422);
        }

        $tokens = $authService->getAuthTokens($user);

        $message = $responseService->createMessage($user->mask());
        $message = $this->tokensToMessage($tokens, $message);
        $message->addOption('expires_in', config('module.auth.jwtauth.exp'));

        return $responseService->send($message);
    }

    /**
     * Sign out
     * @method signout
     * @param  Request         $request
     * @param  ResponseService $responseService
     * @param  AuthService     $authService
     * @return array
     */
    public function signout(Request $request, ResponseService $responseService, AuthService $authService){
        $authService->signout();

        $message = $responseService->createMessage("");
        $message->setHeader('JWTAuth', [
            'clear' => true
        ]);
        return $responseService->send($message);
    }

    public function refresh(Request $request, ResponseService $responseService, AuthService $authService, TokenService $tokenService) {
        $data = $request->json('data');

        $refreshToken = $request->header('JWTRefresh', null);
        $user = $authService->refresh($refreshToken);

        $message = null;
        if ($user != null) {
            $message = $responseService->createMessage($user->toArray());
        }
        $tokens = $authService->getAuthTokens($user);
        $message = $this->tokensToMessage($tokens, $message);
        $message->addOption('expires_in', config('module.auth.jwtauth.exp'));

        return $responseService->send($message);
    }

    /**
     * Validate user email.
     * @method validateUser
     * @param  Request           $request
     * @param  ResponseService   $responseService
     * @param  UserService       $userService
     * @param  ValidationService $validationService
     * @return array
     */
    public function validateUser(Request $request, ResponseService $responseService, UserService $userService, ValidationService $validationService) {
        $data = $request->json()->all();

        // valiadte user input (form)
        if (!$validationService->validate($data, 'form.validate')) {
            return $responseService->use('validation.data');
        }
        // do email validation of a user
        $user = $userService->validate($data['email'], $data['token']);
        if ($user == null) {
            return $responseService->use('validation.data');
        }

        return $responseService->response("OK");
    }
}

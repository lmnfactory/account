<?php

namespace Lmn\Account\Controller;

use Lmn\Core\Lib\Facade\Config;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Lib\Response\ResponseMessage;
use Lmn\Core\Lib\Model\ValidationException;
use Lmn\Core\Lib\Model\ValidationService;

use Lmn\Account\Lib\Auth\CurrentUser;
use Lmn\Account\Lib\User\UserService;
use Lmn\Account\Repository\UserRepository;
use Lmn\Account\Repository\UserPasswordRepository;

class UserController extends Controller {

    public function __construct() {

    }

    public function getList(Request $reqeust, ResponseService $responseService) {
        $data = $request->json('data');

        //$filterService->filter(['user'], $statement);
        $orderService->order('user', $statement);

        $list = $statement->get();

        $response = $responseService->createMessage($list);
        $response->setOption([
            'totalItems' => sizeof($list)
        ]);

        return $responseService->send($response);
    }

    public function getDetail(Request $reqeust, ResponseService $responseService, CurrentUser $currentUser, UserRepository $userRepo){
        $data = $reqeust->json()->all();

        if (!isset($data['public_id'])) {
            $data['public_id'] = $currentUser->getPublicId();
        }

        $user = $userRepo->clear()
            ->criteria('user.by.publicId', ['publicId' => $data['public_id']])
            ->get();

        if ($user == null) {
            return $responseService->response([]);
        }
        return $responseService->response($user->toArray());
    }

    public function update(Request $request, ResponseService $responseService, CurrentUser $currentUser, UserRepository $userRepo, ValidationService $validationService){
        $data = $request->json()->all();

        if (!$validationService->validate($data, 'user.profile.update')) {
            return $responseService->response($validationService->getErrorsArray(), 422);
        }
        
        $allow = ['name', 'surname', 'fullname', 'settings'];
        $updateData = [];
        foreach ($allow as $a) {
            if (!isset($data[$a])) {
                continue;
            }
            $updateData[$a] = $data[$a];
        }
        $user = $userRepo->clear()
            ->criteria('user.by.publicId', ['publicId' => $currentUser->getPublicId()])
            ->update($updateData);

        $user = $userRepo->clear()
            ->criteria('user.by.publicId', ['publicId' => $currentUser->getPublicId()])
            ->get();

        if ($user == null) {
            return $responseService->response([]);
        }
        return $responseService->response($user->toArray());
    }

    public function resetPassword(Request $request, ResponseService $responseService, UserService $userService, ValidationService $validationService) {
        $data = $request->json()->all();

        // valiadte user input (form)
        if (!$validationService->validate($data, 'form.resetpassword')) {
            return $responseService->response($validationService->getErrorsArray(), 422);
        }
        
        $resetPasswordData = [
            'password' => str_random(12)
        ];
        $user = $userService->resetPassword($data['email'], $resetPasswordData['password']);
        $resetPasswordData['name'] = $user->name;
        Mail::send('lmn.account::reset_password_email', $resetPasswordData, function($m) use ($user) {
            $m->from('noreply@veski.sk', 'Veski');
            $m->to($user->email, $user->name)->subject('Tvoje nové heslo - Veski');
        });

        return $responseService->response("OK");
    }

    public function changePassword(Request $request, ResponseService $responseService, CurrentUser $currentUser, ValidationService $validationService, UserPasswordRepository $userPasswordRepo){
        $data = $request->json()->all();

        $user = $currentUser->getUser();
        $data['email'] = $user->email;
        if (!$validationService->validate($data, 'form.changepassword')) {
            return $responseService->response($validationService->getErrorsArray(), 422);
        }

        $userPasswordRepo->clear()
            ->criteria('core.id', ['id' => $currentUser->getId()])
            ->update([
                'password' => $data['new_password']
            ]);

        return $responseService->response("OK");
    }
}

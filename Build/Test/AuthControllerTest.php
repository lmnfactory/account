<?php

namespace Lmn\Account\Build\Test;

use Lmn\Account\Lib\Test\AuthTestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthControllerTest extends AuthTestCase {

    use DatabaseTransactions;

    public function testSignin() {
        $this->json('POST', '/auth/signin', [
                'auth' => [
                    'email' => 'local-1@mail.abc',
                    'password' => 'aaaaaa'
                ],
                'method' => 'lmn-auth',
                'rememberMe' => false
            ])
            ->assertResponseOk()
            ->seeJsonStructure([
                'data' => [ 'pid', 'userEmail' ],
                'header' => [ 'JWTAuth', 'JWTRefresh' ],
                'option' => [ 'expires_in' ]
            ]);
    }

    public function testSigninRemember() {
        $this->json('POST', '/auth/signin', [
                'auth' => [
                    'email' => 'local-1@mail.abc',
                    'password' => 'aaaaaa'
                ],
                'method' => 'lmn-auth',
                'rememberMe' => true
            ])
            ->assertResponseOk()
            ->seeJsonStructure([
                'data' => [ 'pid', 'userEmail' ],
                'header' => [
                    'JWTAuth',
                    'JWTRefresh',
                    'RMAuth' => [ 'series', 'token' ]
                ],
                'option' => [ 'expires_in' ]
            ]);
    }

    public function testSignup() {
        $this->json('POST', '/auth/signup', [
                'name' => 'Peter',
                'surname' => 'Peterson',
                'email' => 'test-1@mail.abc',
                'password' => 'aaaaaa111'
            ])
            ->assertResponseOk();
    }

    public function testSignupInvalidEmail() {
        $this->json('POST', '/auth/signup', [
                'name' => 'Peter',
                'surname' => 'Peterson',
                'email' => 'local-1@mail.abc',
                'password' => 'aaaaaa111'
            ])
            ->assertResponseStatus(422);

        $this->json('POST', '/auth/signup', [
                'name' => 'Peter',
                'surname' => 'Peterson',
                'password' => 'aaaaaa111'
            ])
            ->assertResponseStatus(422);

        $this->json('POST', '/auth/signup', [
                'name' => 'Peter',
                'surname' => 'Peterson',
                'email' => 'local-1@mail',
                'password' => 'aaaaaa111'
            ])
            ->assertResponseStatus(422);
    }

    public function testSignupInvalidPassword() {
        $this->json('POST', '/auth/signup', [
                'name' => 'Peter',
                'surname' => 'Peterson',
                'email' => 'local-1@mail.abc'
            ])
            ->assertResponseStatus(422);

        $this->json('POST', '/auth/signup', [
                'name' => 'Peter',
                'surname' => 'Peterson',
                'email' => 'local-1@mail.abc',
                'password' => 'aaaaaa'
            ])
            ->assertResponseStatus(422);
    }

    public function testSignupInvalidName() {
        $this->json('POST', '/auth/signup', [
                'surname' => 'Peterson',
                'email' => 'local-1@mail.abc',
                'password' => 'aaaaaa111'
            ])
            ->assertResponseStatus(422);
    }

    public function testSignupInvalidSurname() {
        $this->json('POST', '/auth/signup', [
                'name' => 'Peter',
                'email' => 'local-1@mail.abc',
                'password' => 'aaaaaa111'
            ])
            ->assertResponseStatus(422);
    }

    public function testRefresh() {
        $response = $this->signin();

        $this->json('POST', '/auth/refresh', [], [
                'HTTP_JWTRefresh' => $response->getJwtRefresh()
            ])
            ->assertResponseStatus(200)
            ->seeJsonStructure([
                'header' => [ 'JWTAuth', 'JWTRefresh' ],
                'option' => [ 'expires_in' ]
            ]);
    }

    public function testRefreshInvalid() {
        $this->json('POST', '/auth/refresh', [], [
                'HTTP_JWTRefresh' => 'aaaaaaaaaaa'
            ])
            ->assertResponseStatus(500);
    }

    public function testAutosignin() {
        $response = $this->signin();

        $token = $response->getRmAuthToken();
        $this->json('POST', '/auth/autosignin', [], [
                'HTTP_RMAuth' => $token
            ])
            ->assertResponseStatus(200)
            ->seeJsonStructure([
                'header' => [ 'JWTAuth', 'JWTRefresh' ],
                'option' => [ 'expires_in' ]
            ]);
    }

    public function testAutosigninInvalid() {
        $this->json('POST', '/auth/autosignin', [], [
                'HTTP_RMAuth' =>'aaaaaaaaaaa'
            ])
            ->assertResponseStatus(501);
    }

    public function testEmailValidation() {
        $this->json('POST', '/auth/validate', [
                'email' => 'local-3@mail.abc',
                'token' => 'WRMfjfk3Xs5eZtgz9BWTzgs9Yrjg96SX',
            ])
            ->assertResponseStatus(200);
    }

    public function testEmailValidationInvalid() {
        $this->json('POST', '/auth/validate', [
                'email' => 'local-3@mail.abc',
            ])
            ->assertResponseStatus(422);

        $this->json('POST', '/auth/validate', [
                'token' => 'WRMfjfk3Xs5eZtgz9BWTzgs9Yrjg96SX',
            ])
            ->assertResponseStatus(422);

        $this->json('POST', '/auth/validate', [
                'email' => 'local-1@mail.abc',
                'token' => 'WRMfjfk3Xs5eZtgz9BWTzgs9Yrjg96SX',
            ])
            ->assertResponseStatus(422);

        $this->json('POST', '/auth/validate', [
                'email' => 'local-3@mail.abc',
                'token' => 'WRMffk3Xs5eZtgz9BWTzgs9Yrjg96SX',
            ])
            ->assertResponseStatus(422);
    }

    public function testSignout() {
        $this->authJson('POST', 'auth/signout', [], [
            'HTTP_RMAuth' => ""
        ])
            ->assertResponseOk();
    }

    public function testSignoutInvalid() {
        $this->json('POST', 'auth/signout', [])
            ->assertResponseStatus(401);
    }
}

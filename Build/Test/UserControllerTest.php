<?php

namespace Lmn\Account\Build\Test;

use Lmn\Account\Lib\Test\AuthTestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserControllerTest extends AuthTestCase {

    use DatabaseTransactions;

    public function testGetDetail() {
        $this->authJson('POST', '/api/user/profile', [])
            ->assertResponseStatus(200);
    }

    public function testGetDetailInvalid() {
        $this->json('POST', '/api/user/profile', [])
            ->assertResponseStatus(401);
    }

    public function testUpdateProfile() {
        $this->authJson('POST', '/api/user/profile/update', [
                'name' => 'Meno',
                'surname' => 'Priezvisko',
                'faculty_id' => '1',
                'degree_id' => '1',
                'studyyear' => '2'
            ])
            ->assertResponseStatus(200);
    }

    public function testUpdateProfileInvalid() {
        $this->json('POST', '/api/user/profile/update', [
                'name' => 'Meno',
                'surname' => 'Priezvisko',
                'faculty_id' => '1',
                'degree_id' => '1',
                'studyyear' => '2'
            ])
            ->assertResponseStatus(401);

        $this->authJson('POST', '/api/user/profile/update', [
                'name' => 'Meno',
                'faculty_id' => '1',
                'degree_id' => '1',
                'studyyear' => '2'
            ])
            ->assertResponseStatus(422);

        $this->authJson('POST', '/api/user/profile/update', [
                'surname' => 'Priezvisko',
                'faculty_id' => '1',
                'degree_id' => '1',
                'studyyear' => '2'
            ])
            ->assertResponseStatus(422);

        $this->authJson('POST', '/api/user/profile/update', [])
            ->assertResponseStatus(422);
    }

    public function testChangePassword() {
        $this->authJson('POST', '/user/change_password', [
                'password' => 'aaaaaa',
                'new_password' => 'bbbbbb'
            ])
            ->assertResponseStatus(200);
    }

    public function testChangePasswordInvalid() {
        $this->json('POST', '/user/change_password', [
                'password' => 'aaaaaa',
                'new_password' => 'bbbbbb'
            ])
            ->assertResponseStatus(401);

        $this->authJson('POST', '/user/change_password', [
                'password' => 'aaaaa',
                'new_password' => 'bbbbbb'
            ])
            ->assertResponseStatus(422);

        $this->authJson('POST', '/user/change_password', [
                'new_password' => 'bbbbbb'
            ])
            ->assertResponseStatus(422);

        $this->authJson('POST', '/user/change_password', [
                'password' => 'aaaaa',
            ])
            ->assertResponseStatus(422);
    }

    public function testResetPassword() {
        $this->json('POST', '/user/reset_password', [
                'email' => 'local-3@mail.abc'
            ])
            ->assertResponseStatus(200);
    }

    public function testResetPasswordInvalid() {
        $this->json('POST', '/user/reset_password', [
                'email' => 'local-3@mail.a'
            ])
            ->assertResponseStatus(422);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->char('public_id', 64);
            $table->char('private_id', 32);
            $table->char('email', 128);
            $table->char('name', 128);
            $table->char('surname', 128);
            $table->char('fullname', 255);
            $table->char('password', 128)->nullable();
            $table->char('isic', 20)->nullable();
            $table->char('validationtoken', 32)->nullable();
            $table->char('series', 32)->nullable();
            $table->boolean('active')->default(1);
            $table->boolean('valid')->default(0);
            $table->datetime('lastsignin')->nullable();
            $table->timestamps();

            $table->index('public_id');
            $table->index('private_id');
            $table->index('email');
            $table->index('fullname');
            $table->index('validationtoken');
            $table->index('series');
            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}

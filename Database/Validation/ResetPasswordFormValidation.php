<?php

namespace Lmn\Account\Database\Validation;

use Lmn\Core\Lib\Model\LaravelValidation;

class ResetPasswordFormValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'email' => 'required|exists:user,email'
        ];
    }
}

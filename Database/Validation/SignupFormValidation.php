<?php

namespace Lmn\Account\Database\Validation;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Model\ModelValidation;
use Lmn\Core\Lib\Model\ModelValidationError;

class SignupFormValidation implements ModelValidation {

    private $validationService;

    public function __construct() {
        $this->validationService = \App::make(ValidationService::class);
    }

    public function validate($model) {
        $validator = \Validator::make($model, [
            'name' => 'required|max:128',
            'surname' => 'required|max:128',
            'email' => 'required|max:128|email|unique:user,email',
            'password' => 'required|min:6|max:40'
        ]);

        if ($validator->fails()) {
            return  $this->validationService->laravelErrorToError($validator);
        }

        return true;
    }
}

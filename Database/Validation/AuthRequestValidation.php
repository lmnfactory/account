<?php

namespace Lmn\Account\Database\Validation;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Model\LaravelValidation;

class AuthRequestValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'auth' => 'required'
        ];
    }
}

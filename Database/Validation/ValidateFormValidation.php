<?php

namespace Lmn\Account\Database\Validation;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Model\LaravelValidation;
use Lmn\Core\Lib\Model\ModelValidationError;

class ValidateFormValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'token' => 'required',
            'email' => 'required'
        ];
    }
}

<?php

namespace Lmn\Account\Database\Validation;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Model\LaravelValidation;
use Lmn\Core\Lib\Model\ModelValidationError;

class UserValidateValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'email' => 'required|email',
            'validationtoken' => 'required|validationToken:email'
        ];
    }
}

<?php

namespace Lmn\Account\Database\Validation;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Model\ModelValidation;
use Lmn\Core\Lib\Model\ModelValidationError;

class UserCreateValidation implements ModelValidation {

    private $validationService;

    public function __construct() {
        $this->validationService = \App::make(ValidationService::class);
    }

    public function validate($model) {
        $validator = \Validator::make($model, [
            'name' => 'required|max:128',
            'surname' => 'required|max:128',
            'email' => 'required|max:128|email|unique:user',
            'public_id' => 'required|unique:user',
            'private_id' => 'required|unique:user'
        ]);

        if ($validator->fails()) {
            return  $this->validationService->laravelErrorToError($validator);
        }

        return true;
    }
}

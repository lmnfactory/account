<?php

namespace Lmn\Account\Database\Validation;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Model\LaravelValidation;
use Lmn\Core\Lib\Model\ModelValidationError;

class SigninFormValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'email' => 'required|exists:user,email|validUser',
            'password' => 'required|passwordCheck:email',
        ];
    }
}

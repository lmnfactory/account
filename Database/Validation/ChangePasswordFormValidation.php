<?php

namespace Lmn\Account\Database\Validation;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Model\LaravelValidation;
use Lmn\Core\Lib\Model\ModelValidationError;

class ChangePasswordFormValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'email' => 'required|exists:user,email|validUser',
            'password' => 'required|passwordCheck:email',
            'new_password' => 'required|min:6|max:40'
        ];
    }
}

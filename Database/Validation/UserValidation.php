<?php

namespace Lmn\Account\Database\Validation;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Model\ModelValidation;
use Lmn\Core\Lib\Model\ModelValidationError;

class UserValidation implements ModelValidation {

    private $validationService;

    public function __construct() {
        $this->validationService = \App::make(ValidationService::class);
    }

    public function validate($model) {
        $validator = \Validator::make($model, [
            'name' => 'max:128',
            'surname' => 'max:128',
            'fullname' => 'required|max:255',
            'email' => 'required|max:128|email',
            'public_id' => 'required',
            'private_id' => 'required',
            'series' => 'required'
        ]);

        if ($validator->fails()) {
            return  $this->validationService->laravelErrorToError($validator);
        }

        return true;
    }
}

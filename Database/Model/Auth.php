<?php

namespace Lmn\Account\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Auth extends Model {

    protected $table = 'auth';

    protected $fillable = ['user_id', 'method', 'uid'];
}

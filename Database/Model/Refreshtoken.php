<?php

namespace Lmn\Account\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Refreshtoken extends Model {

    protected $table = 'refreshtoken';

    protected $fillable = ['user_id', 'tid', 'expires_at'];
}

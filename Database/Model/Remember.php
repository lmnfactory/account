<?php

namespace Lmn\Account\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Remember extends Model {

    protected $table = 'remember';

    protected $fillable = ['user_id', 'series', 'token', 'expires_at'];

    public function mask(){
        return [
            'series' => $this->series,
            'token' => $this->token
        ];
    }

    public function getExpiresTime() {
        return strtotime($this->expires);
    }
}

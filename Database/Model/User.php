<?php

namespace Lmn\Account\Database\Model;

use Illuminate\Database\Eloquent\Model;
use Lmn\Core\Lib\Facade\Generator;

class User extends Model {

    protected $table = 'user';

    protected $fillable = ['email', 'password', 'name', 'surname', 'fullname', 'isic', 'valid', 'validationtoken', 'lastsignin'];

    protected $hidden = ['password', 'private_id', 'series', 'validationtoken', 'isic'];

    public function mask(){
        return [
            'pid' => $this->public_id,
            'userEmail' => $this->email,
            'userName' => $this->name,
            'userSurname' => $this->surname,
            'userFullame' => $this->fullname,
            'isActive' => $this->active,
            'isValid' => $this->valid
        ];
    }
}

<?php

namespace Lmn\Account\Database\Seed;

use App;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {

    private function dev() {
        \DB::table('user')->insert([
            [
                'id' => 1,
                'public_id' => 'UZkWO94OoICtKUbvZo1XHS09Ou1Im2Ak',
                'private_id' => '0mhJKZbp9NS35dEIIa3YOgbTDbOze0An',
                'email' => 'local-1@mail.abc',
                'name' => 'Karol',
                'surname' => 'Strohon',
                'fullname' => 'Karol Strohon',
                'password' => password_hash('aaaaaa', PASSWORD_DEFAULT),
                'series' => 'eBWTzgs9Yrjg9Ztgz9WRMfjfk3Xs56SX',
                'active' => 1,
                'valid' => 1
            ],
            [
                'id' => 2,
                'public_id' => 'bvZo1XHS09Ou1Im2AkUZkWO94OoICtKU',
                'private_id' => '35dEIIa3YOgbTDbOze0An0mhJKZbp9NS',
                'email' => 'local-2@mail.abc',
                'name' => 'Adriana',
                'surname' => 'Vlhova',
                'fullname' => 'Adriana Vlhova',
                'password' => password_hash('aaaaaa', PASSWORD_DEFAULT),
                'series' => 'Ztgz9WRMfjfk3Xs5eBWTzgs9Yrjg96SX',
                'active' => 1,
                'valid' => 1
            ]
        ]);
        \DB::table('user')->insert([
            [
                'id' => 3,
                'public_id' => '1XHS09Ou1Im2AkbvZoUZkWO94OoICtKU',
                'private_id' => 'IIa3YOgbTDb35dEOze0An0mhJKZbp9NS',
                'email' => 'local-3@mail.abc',
                'name' => 'Marian',
                'surname' => 'Sruptik',
                'fullname' => 'Marian Sruptik',
                'password' => password_hash('aaaaaa', PASSWORD_DEFAULT),
                'series' => 'z9WRMfjfk3Xs5eBWTzgs9YrZtgjg96SX',
                'active' => 1,
                'valid' => 0,
                'validationtoken' => 'WRMfjfk3Xs5eZtgz9BWTzgs9Yrjg96SX'
            ]
        ]);

        \DB::table('auth')->insert([
            [
                'id' => 1,
                'user_id' => 1,
                'method' => 'lmn-auth',
                'uid' => '0mhJKZbp9NS35dEIIa3YOgbTDbOze0An',
            ],
            [
                'id' => 2,
                'user_id' => 2,
                'method' => 'lmn-auth',
                'uid' => '35dEIIa3YOgbTDbOze0An0mhJKZbp9NS',
            ],
            [
                'id' => 3,
                'user_id' => 3,
                'method' => 'lmn-auth',
                'uid' => 'IIa3YOgbTDb35dEOze0An0mhJKZbp9NS',
            ]
        ]);
    }

    private function prod() {
        
    }

    public function run() {
        $env = App::environment();
        if ($env == "development") {
            $this->dev();
        } else if ($env == "production") {
            $this->prod();
        }
    }
}

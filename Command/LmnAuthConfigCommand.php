<?php

namespace Lmn\Account\Command;

use Illuminate\Console\Command;
use Lmn\Core\Lib\Facade\Config;

class LmnAuthConfigCommand extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lmn:auth-config';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize authentification configuration.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = "module";
        $path = config_path($file.'.php');

        if (!file_exists($path)){
            $this->error($file.".php config file does not exists. create it by calling php artisan lmn:module-init");
            return;
        }

        $config = [
            'auth' => [
                'iss' => 'lmn.auth'
            ]
        ];
        $current = include($path);

        foreach ($config as $key => $value) {
            if (!isset($current[$key])){
                $current[$key] = $config[$key];
            }
        }

        $content = "<?php".PHP_EOL.
            PHP_EOL.
            "return ". Config::print($current).";";

        file_put_contents($path, $content);

        $this->info($file.".php config file updated. You can configure it now.");
    }
}

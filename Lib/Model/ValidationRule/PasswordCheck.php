<?php

namespace Lmn\Account\Lib\Model\ValidationRule;

use Lmn\Core\Lib\Model\ValidationRule\ValidationRule;

use Lmn\Account\Lib\User\UserService;
use Lmn\Account\Database\Model\User;

class PasswordCheck implements ValidationRule {

    public function __construct() {

    }

    /**
     * Password is valid if email address (which filed name is passed as a parameter) match with password in database.
     * @method rule
     * @param  string $attribute field name
     * @param  string $value value of the field being validated
     * @param  array $parameters other parameter
     * @param  Validator $validator validator instance
     * @return boolean
     */
    public function rule($attribute, $value, $parameters, $validator) {
        $userService = \App::make(UserService::class);
        $data = $validator->getData();
        $email = $data[$parameters[0]];
        try {
            $user = User::where('email', '=', $email)
                ->whereNotNull('password')
                ->where('active', '=', true)
                ->where('valid', '=', true)->first();

            if ($user == null) {
                return false;
            }
            if (!password_verify($value, $user->password)) {
                return false;
            }

            return true;
        }
        catch (\Exception $ex){

        }
        return false;
    }
}

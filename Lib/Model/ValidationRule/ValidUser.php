<?php

namespace Lmn\Account\Lib\Model\ValidationRule;

use Lmn\Core\Lib\Model\ValidationRule\ValidationRule;

use Lmn\Account\Lib\User\UserService;
use Lmn\Account\Database\Model\User;

class ValidUser implements ValidationRule {

    public function __construct() {

    }

    /**
     * Valid user is user that is actie is valid and email address equals to email address that is being validated.
     * @method rule
     * @param  string $attribute field name
     * @param  string $value value of the field being validated
     * @param  array $parameters other parameter
     * @param  Validator $validator validator instance
     * @return boolean
     */
    public function rule($attribute, $value, $parameters, $validator) {
        $userService = \App::make(UserService::class);
        try {
            $user = User::where('email', '=', $value)
                ->where('active', '=', true)
                ->where('valid', '=', true)->first();

            return ($user != null);
        }
        catch (\Exception $ex){

        }
        return false;
    }
}

<?php

namespace Lmn\Account\Lib\Model\ValidationRule;

use Lmn\Core\Lib\Model\ValidationRule\ValidationRule;

use Lmn\Account\Lib\User\UserService;
use Lmn\Account\Database\Model\User;

class ValidationToken implements ValidationRule {

    public function __construct() {

    }

    /**
     * Validation token is valid if email address (which field name is passed as a parameter) and validation token match in database.
     * @method rule
     * @param  string $attribute field name
     * @param  string $value value of the field being validated
     * @param  array $parameters other parameter
     * @param  Validator $validator validator instance
     * @return boolean
     */
    public function rule($attribute, $value, $parameters, $validator) {
        $userService = \App::make(UserService::class);
        $data = $validator->getData();
        $email = $data[$parameters[0]];
        try {
            $user = User::where('email', '=', $email)
                ->where('validationtoken', '=', $value)->first();

            return ($user != null);
        }
        catch (\Exception $ex){

        }
        return false;
    }
}

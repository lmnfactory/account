<?php

namespace Lmn\Account\Lib\Exception;

use Lmn\Core\Lib\Exception\ExceptionHandler;
use Lmn\Core\Lib\Response\ResponseService;

use Lmn\Account\Lib\Auth\UnauthorizedException;

class AuthorizationFailedExceptionHandler implements ExceptionHandler {

    public function __construct() {

    }

    public function report(\Exception $ex) {

    }

    public function render($request, \Exception $ex, ResponseService $responseService) {
        return $responseService->response($ex->getMessage(), 501);
    }
}

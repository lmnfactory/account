<?php

namespace Lmn\Account\Lib\Facebook;

use Lmn\Core\Lib\Model\LaravelValidation;

class FacebookAuthValidation extends LaravelValidation {

    public function __construct() {

    }

    public function getRules($data) {
        return [
            'accessToken' => 'required'
        ];
    }
}

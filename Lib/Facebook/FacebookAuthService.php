<?php

namespace Lmn\Account\Lib\Facebook;

use Lmn\Core\Lib\Facade\Config;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Account\Lib\Auth\AuthMethod;
use Lmn\Account\Lib\Auth\AuthService;
use Lmn\Account\Lib\User\UserService;
use Lmn\Account\Lib\User\SigninException;

use Lmn\Account\Lib\Auth\InvalidAuthDataException;
use Lmn\Account\Lib\Auth\AuthorizationFailedException;
use Lmn\Account\Database\Model\User;
use Lmn\Account\Database\Model\Auth;

use Facebook\Facebook;
use Facebook\GraphNodes\GraphUser;

class FacebookAuthService implements AuthMethod{

    const KEY = "facebook-oauth";

    private $fb;
    private $config;
    private $authService;
    private $userService;

    public function __construct(){
        $this->authService = \App::make(AuthService::class);
        $this->userService = \App::make(UserService::class);

        $this->config = Config::get("lmn.account.access.facebook", false);
        $this->fb = new Facebook([
            'app_id' => $this->config['app_id'],
            'app_secret' => $this->config['app_secret'],
            'default_graph_version' => $this->config['graph_version'],
        ]);
    }

    /**
     * Register new user and create new authentification record for that user.
     * @method register
     * @param  array   $data user and auth data
     * @return Model\Auth
     */
    public function register($data){
        $auth = $this->authService->register([
            'user_id' => $data['user_id'],
            'method' => self::KEY,
            'uid' => $data['sub']
        ]);

        return $auth;
    }

    /**
     * Determine is authentification data should be handled by this service. True if method section of $requestData equals to this.KEY.
     * @method isMyClient
     * @param  array     $requestData data part of request
     * @param  Model\User     $currUser
     * @return boolean true if 'method' section of $requestData equals to self::KEY.
     */
    public function isMyClient($requestData, $currUser = null){
        if (!isset($requestData['method'])){
            return false;
        }

        return ($requestData['method'] == self::KEY);
    }

    /**
     * Verify data with google server.
     * @method verify
     * @param  string $id_token user id_token from facebook.
     * @throws AuthorizationFailedException
     * @return GraphUser
     */
    public function verify($id_token){
        $this->fb->setDefaultAccessToken($id_token);

        try {
            $response = $this->fb->get('/me?fields=id,name,email,first_name,middle_name,last_name');
            $userNode = $response->getGraphUser();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            throw new AuthorizationFailedException("Permission not granted.");
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            throw new AuthorizationFailedException("Permission not granted.");
        }

        if ($userNode == null) {
            throw new AuthorizationFailedException("Permission not granted.");
        }

        return $userNode;
    }

    /**
     * Sign in user with authentification data.
     * @method signin
     * @param  array $authData auth part of request data
     * @throws SigninException
     * @return Model\User
     */
    public function signin($authData) {
        try {
            $validationService = \App::make(ValidationService::class);
            $validationService->systemValidateOrFail($authData, 'auth.facebook');
            $facebookUser = $this->verify($authData['accessToken']);

            $user = $this->authService->authorize(self::KEY, $facebookUser->getId());
            if ($user == null) {
                //TODO transaction
                $name = $facebookUser->getFirstName();
                if ($facebookUser->getMiddleName() != null) {
                    $name .= " ".$facebookUser->getMiddleName();
                }
                $surname = $facebookUser->getLastName();
                $user = $this->userService->register([
                    'name' => $name,
                    'surname' => $surname,
                    'fullname' => $facebookUser->getName(),
                    'email' => $facebookUser->getEmail(),
                    'valid' => true
                ]);

                $auth = $this->register([
                    'sub' => $facebookUser->getId(),
                    'user_id' => $user->id
                ]);
            }

            return $user;
        }
        catch (\Esception $ex) {
            throw new SigninException("Sign in failed.", $ex);
        }
    }

    /**
     * Sign out is handled by AuthService signout method.
     * @method signout
     */
    public function signout(){

    }

}

<?php

namespace Lmn\Account\Lib\Auth;

use Lmn\Core\Lib\JWT\JWTUser;
use Lmn\Core\Lib\JWT\JWTService;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Model\ValidationException;

use Lmn\Account\Repository\UserRepository;
use Lmn\Account\Repository\AuthRepository;

use Lmn\Account\Repository\Criteria\Auth\AuthorizeCriteria;
use Lmn\Account\Repository\RefreshtokenRepository;
use Lmn\Core\Repository\Criteria\IdCriteria;
use Lmn\Core\Exception\SystemException;

use Lmn\Account\Lib\Auth\AuthMethod;
use Lmn\Account\Lib\Auth\SigninException;
use Lmn\Account\Lib\Auth\SignoutException;
use Lmn\Account\Lib\Auth\Token\TokenService;

use Lmn\Account\Lib\User\UserService;

use Lmn\Account\Lib\Remember\RememberService;

use Lmn\Account\Database\Model\Auth;
use Lmn\Account\Database\Model\User;
use Firebase\JWT\ExpiredException;

/**
 * @property User $currentUser
 * @property AuthRepository $authRepo
 * @property UserRepository $userRepo
 * @property RefreshtokenRepository $refreshtokenRepo
 * @property ValidationService $ValidationService
 * @property RememberService $rememberService
 * @property JWTService $jwtService
 * @property TokenService $tokenService
 */
class AuthService implements AuthMethod {

    private $currentUser;
    private $method;
    private $authRepo;
    private $userService;
    private $userRepo;
    private $refreshtokenRepo;
    private $validationService;
    private $rememberService;
    private $hwtService;
    private $tokenService;

    public function __construct(UserService $userService, UserRepository $userRepo, AuthRepository $authRepo, RefreshtokenRepository $refreshtokenRepo, ValidationService $validationService, RememberService $rememberService, JWTService $jwtService, TokenService $tokenService){
        $this->currentUser = null;
        $arrayService = \App::make('arrayService');
        $this->authRepo = $authRepo;
        $this->userRepo = $userRepo;
        $this->refreshtokenRepo = $refreshtokenRepo;
        $this->userService = $userService;
        $this->validationService = $validationService;
        $this->rememberService = $rememberService;
        $this->jwtService = $jwtService;
        $this->tokenService = $tokenService;
        $this->method = $arrayService->make('unorderedSingleton');
    }

    private function signinSuccess($user) {
        $this->userService->updateLastSignin($user);
    }

    /**
     * Add another method of authentification.
     * @method addMethod
     * @param  mixed    $method somthing that can be transfered into AuthMethod instance (Closure, class name, function name, ...)
     */
    public function addMethod($method){
        $this->method->add($method);
    }

    /**
     * Retrieve authentification method according to request data
     * @method getMethod
     * @param  array    $requestData data part of the request
     * @throws NoAuthServiceException
     * @return AuthMethod
     */
    public function getMethod($requestData){
        $all = $this->method->getAll();
        foreach ($all as $key => $i) {
            if ($i->isMyClient($requestData, $this->getCurrentUser())){
                return $i;
            }
        }

        throw new NoAuthServiceException("No authentification service avalible");
    }

    /**
     * Retrieve currently signed in user
     * @method getCurrentUser
     * @return Model\User         or null if nobody is sigen in
     */
    public function getCurrentUser(){
        return $this->currentUser;
    }

    /**
     * Check if there is any user currently signed in
     * @method hasCurrentUser
     * @return boolean
     */
    public function hasCurrentUser(){
        return ($this->getCurrentUser() != null);
    }

    /**
     * Check if there is any user currently signed in
     * @method isSignedIn
     * @return boolean
     */
    public function isSignedIn() {
        return $this->hasCurrentUser();
    }

    /**
     * Set which user is currently signed in
     * @method setCurrentUser
     * @param Model\User $user
     */
    public function setCurrentUser($user) {
        $this->currentUser = $user;
    }

    /**
     * Sign in using JWTAuth token and retrieve information about user. If there is no JWTAuth, then try to authorize by remember me token (RMAuth).
     * @method autoSignIn
     */
    public function autoSignin($token = null) {
        try {
            $user = $this->checkJWTAuth($token);
            $this->setCurrentUser($user);
            return true;
        }
        catch (\Exception $ex){
            return false;
        }
    }

    public function rememberSignin($token = null){
        $user = null;
        try {
            $user = $this->rememberService->signin($token);
        }
        catch (\Exception $ex) {
            throw new AuthorizationFailedException("Authorization failed.", $ex->getCode(), $ex);
        }

        if ($user == null) {
            throw new AuthorizationFailedException("Authorization failed.");
        }
        $this->setCurrentUser($user);
        $this->signinSuccess($user);
        return $user;
    }

    /**
     * [checkJWTAuth description]
     * @method checkJWTAuth
     * @param  [type]       $token [description]
     * @throws ExpiredException
     * @return [type]              [description]
     */
    public function checkJWTAuth($token) {
        $authToken = $this->jwtService->decode($token);
        if ($authToken == null) {
            return null;
        }

        $user = $this->userService->getByPrivateId($authToken->sub);
        return $user;
    }

    /**
     * Register new authentification. User can authentificate with multiple metods (google, facebook, native, ...)
     * @method register
     * @param  array   $data unified authentification data
     * @return Lmn\Account\Model\Auth
     */
    public function register($data){
        return $this->authRepo->clear()
            ->create($data);
    }

    /**
     * Retrieve user by authorization information.
     * @method authorize
     * @param  string    $method authorization method (google, lmn, facebook, ...)
     * @param  string    $uid    unique user identifier
     * @return Model\User
     */
    public function authorize($method, $uid){
        $auth = $this->authRepo->clear()
            ->criteria('auth.authorize', ['method' => $method, 'uid' => $uid])
            ->get();

        if ($auth == null){
            return null;
        }

        $user = $this->userRepo->clear()
            ->criteria('core.id', ['id' => $auth->user_id])
            ->get();

        return $user;
    }

    public function isMyClient($requestData, $currUser = null){
        return true;
    }

    /**
     * Sign in user. This method will choose from registered signin methods and use it to sign in user.
     * @method signin
     * @param  array $requestData data value from request
     * @throws ValidationException
     * @throws SigninException
     * @return Model\User
     */
    public function signin($requestData){
        if ($this->hasCurrentUser()){
            throw new SigninException("User is already signed in.");
        }

        $this->validationService->validateOrFail($requestData, 'auth.request');

        $user = $this->getMethod($requestData)->signin($requestData['auth']);
        $this->setCurrentUser($user);

        if (isset($requestData['rememberMe']) && $requestData['rememberMe'] === true) {
            $this->rememberService->remember($user);
        }

        $this->signinSuccess($user);

        return $user;
    }

    /**
     * Sign out current user
     * @method signout
     * @throws SignoutException
     */
    public function signout(){
        if (!$this->hasCurrentUser()){
            throw new SignoutException("User is not signed in.");
        }

        $this->setCurrentUser(null);

        $this->rememberService->signout();
    }

    public function refresh($token = null) {
        if ($token == null) {
            throw new SystemException("Refresh token is missing.");
        }

        $refreshToken = $this->tokenService->decode('jwt.refresh', $token);
        $user = $this->userService->getByPrivateId($refreshToken->get('sub'));

        $data = [
            'user_id' => $user->id,
            'tid' => $refreshToken->get('jti'),
            'expires_at' => $refreshToken->get('exp')
        ];
        $this->validationService->validate($data, 'refreshtoken.refresh');

        $this->refreshtokenRepo->clear()
            ->create($data);

        return $user;
    }

    public function getAuthTokens($user) {
        if ($user == null) {
            return [];
        }

        $tokens = [];
        $tokens['JWTAuth'] = $this->tokenService->create('jwt.auth')
            ->set('sub', $user->private_id);
        $tokens['JWTRefresh'] = $this->tokenService->create('jwt.refresh')
            ->set('sub', $user->private_id);

        if ($this->rememberService->hasChanged()){
            $tokens['RMAuth'] = $this->rememberService->getToken();
        }

        return $tokens;
    }
}

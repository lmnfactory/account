<?php

namespace Lmn\Account\Lib\Auth;
use Lmn\Account\Model\User;
use Lmn\Account\Lib\Auth\SigninException;
use Lmn\Account\Lib\Auth\SignoutException;
use Lmn\Core\Lib\Model\ValidationException;
use Lmn\Account\Database\Model\Auth;

interface AuthMethod {
    /**
     * Resolve if data provided from request are ment for this implementation of authentification.
     * @method isMyClient
     * @param  array     $requestData data value from request
     * @param  User     $currUser    or null if no user is currently loged in
     * @return boolean true if this request belongs to you, false otherwise
     */
    public function isMyClient($requestData, $currUser = null);

    /**
     * Register new user
     * @method register
     * @param  array   $data user data
     * @return Model\Auth         newly registered authentification
     */
    public function register($data);

    /**
     * Sign in user.
     * @method signin
     * @param  array $authData authentification part of request data
     * @throws SigninException
     * @throws ValidationException
     * @return User           signed in user
     */
    public function signin($authData);

    /**
     * Sign out current user.
     * @method signout
     * @throws SignoutException
     */
    public function signout();
}

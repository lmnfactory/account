<?php

namespace Lmn\Account\Lib\Auth\Token;

interface TokenGenerator {
    public function create();
    public function set($index, $value);
    public function get($index = null);
    public function has($index);
    public function isValid();
    public function encode();
    public function decode($hash);
}

<?php

namespace Lmn\Account\Lib\Auth\Token;

use Lmn\Account\Lib\Auth\Token\TokenGenerator;

abstract class Token implements TokenGenerator{

    private $tokenData;

    public function __construct() {
        $this->tokenData = [];
    }

    public function create() {

    }

    public function set($index, $value) {
        $this->tokenData[$index] = $value;
        return $this;
    }

    public function get($index = null) {
        if ($index == null) {
            return $this->tokenData;
        }
        return $this->tokenData[$index];
    }

    public function has($index) {
        return (isset($this->tokenData[$index]));
    }

    public function isValid() {
        return true;
    }
}

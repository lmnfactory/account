<?php

namespace Lmn\Account\Lib\Auth\Token;
use Lmn\Core\Lib\Instance\InstanceService;
use Lmn\Core\Lib\Map\MapHandlerInterface;

/**
 * @property InstanceService $instanceService
 */
class TokenService {

    private $instanceService;
    private $_tokens;

    public function __construct(InstanceService $instanceService, MapHandlerInterface $map) {
        $this->instanceService = $instanceService;
        $this->_tokens = $map;
    }

    public function add($tokenName, $token) {
        $this->_tokens->add($tokenName, $token);
    }

    public function get($tokenName) {
        $blueprint = $this->_tokens->get($tokenName);
        return $this->instanceService->make($blueprint);
    }

    public function create($tokenName) {
        $token = $this->get($tokenName);
        $token->create();
        return $token;
    }

    public function decode($tokenName, $hash) {
        $token = $this->get($tokenName);
        $token->decode($hash);
        return $token;
    }
}

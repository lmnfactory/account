<?php

namespace Lmn\Account\Lib\Auth\Token;

use Lmn\Core\Lib\JWT\JWTService;
use Lmn\Account\Lib\Auth\Token\Token;

class JWToken extends Token {

    private $tokenData;
    private $jwtService;

    public function __construct(JWTService $jwtService) {
        $this->jwtService = $jwtService;
        $this->tokenData = [];
    }

    public function set($index, $value) {
        $this->tokenData[$index] = $value;
        return $this;
    }

    public function get($index = null) {
        if ($index == null) {
            return $this->tokenData;
        }
        return $this->tokenData[$index];
    }

    public function has($index) {
        return (isset($this->tokenData[$index]));
    }

    public function encode() {
        return $this->jwtService->encode($this->get());
    }

    public function decode($hash) {
        $data = $this->jwtService->decode($hash);

        foreach ($data as $key => $value) {
            $this->set($key, $value);
        }
    }
}

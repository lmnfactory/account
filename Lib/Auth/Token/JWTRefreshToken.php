<?php

namespace Lmn\Account\Lib\Auth\Token;

use Lmn\Account\Lib\Auth\Token\JWToken;
use Lmn\Core\Lib\JWT\JWTService;

class JWTRefreshToken extends JWToken {

    private $token;

    public function __construct(JWTService $JWTService) {
        parent::__construct($JWTService);
    }

    public function create() {
        //IAT - issued at
        $this->set('iat', time());

        //EXP - expiration
        $exp = config("module.auth.jwtrefresh.exp", 2592000);
        $this->set('exp', $this->get('iat') + $exp);

        //ISS - issuer
        $this->set('iss', config("module.auth.jwtauth.iss", "lmn.auth"));

        //AUD - audience
        $this->set('aud', config("module.auth.jwtauth.aud", "lmn"));
    }

    public function encode() {
        if (!$this->has('jti')) {
            //JTI - JWT id
            $this->set('jti', bcrypt($this->get('sub').$this->get('iat')));
        }
        return parent::encode();
    }
}

<?php

namespace Lmn\Account\Lib\Auth;

class UnauthorizedException extends \Exception {

    public function __construct($message, $code = 401, $previous = null){
        parent::__construct($message, $code, $previous);
    }
}

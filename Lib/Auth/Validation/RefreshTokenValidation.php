<?php

namespace Lmn\Account\Lib\Auth\Validation;

use Lmn\Core\Lib\Model\LaravelValidation;

class RefreshTokenValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'user_id' => 'required|exists:user,id',
            'tid' => 'required|unique:refreshtoken,tid',
            'expires_at' => 'required|gt:'.time()
        ];
    }
}

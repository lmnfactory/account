<?php

namespace Lmn\Account\Lib\Auth;

class NoAuthServiceException extends \Exception {

    public function __construct($message, $code = null, $previous = null){
        parent::__construct($message, $code, $previous);
    }
}

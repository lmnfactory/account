<?php

namespace Lmn\Account\Lib\Auth;

use Illuminate\Http\Request;
use Lmn\Core\Lib\JWT\JWTUser;
use Lmn\Core\Lib\JWT\JWTService;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Model\ValidationException;

use Lmn\Account\Lib\User\IUser;

use Lmn\Account\Lib\Auth\AuthService;
use Lmn\Account\Database\Model\User;

/**
 * @property User $user
 */
class CurrentUser implements IUser {

    private $user;
    private $authService;

    public function __construct(AuthService $authService){
        $this->authService = $authService;
        $this->user = $authService->getCurrentUser();
    }

    private function hasUser() {
        // Because some DI can happen before we know if the user is logged in (for example in boot method of module provider)
        $this->user = $this->authService->getCurrentUser();
        return ($this->user != null);
    }

    public function getId() {
        if (!$this->hasUser()) {
            return 0;
        }

        return $this->user->id;
    }

    public function getPublicId() {
        if (!$this->hasUser()) {
            return "";
        }

        return $this->user->public_id;
    }

    public function getFullname() {
        if (!$this->hasUser()) {
            return "";
        }

        return $this->user->fullname;
    }

    public function getUser()
    {
        if (!$this->hasUser()) {
            return null;
        }
        return $this->user;
    }
}

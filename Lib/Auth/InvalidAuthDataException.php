<?php

namespace Lmn\Account\Lib\Auth;

class InvalidAuthDataException extends \Exception {

    public function __construct($message){
        parent::__construct($message);
    }
}

<?php

namespace Lmn\Account\Lib\Auth;

class UserNotVerifiedException extends \Exception {

    public function __construct($message, $code = null, $previous = null){
        parent::__construct($message, $code, $previous);
    }
}

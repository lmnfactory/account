<?php

namespace Lmn\Account\Lib\Auth;

class SigninException extends \Exception {

    public function __construct($message, $previous = null, $code = 400){
        parent::__construct($message, $code, $previous);
    }
}

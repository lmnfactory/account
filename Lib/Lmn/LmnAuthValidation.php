<?php

namespace Lmn\Account\Lib\Lmn;

use Lmn\Core\Lib\Model\LaravelValidation;

class LmnAuthValidation extends LaravelValidation {

    public function __construct() {

    }

    public function getRules($data) {
        return [
            'email' => 'required|exists:user,email|validUser',
            'password' => 'required|passwordCheck:email',
        ];
    }
}

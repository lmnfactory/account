<?php

namespace Lmn\Account\Lib\Lmn;

use Lmn\Core\Lib\Facade\Config;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Model\ValidationException;
use Lmn\Core\Exception\SystemException;
use Lmn\Account\Lib\Auth\AuthMethod;
use Lmn\Account\Lib\Auth\AuthService;
use Lmn\Account\Lib\User\UserService;
use Lmn\Account\Lib\Auth\InvalidAuthDataException;
use Lmn\Account\Lib\Auth\AuthorizationFailedException;
use Lmn\Account\Lib\Auth\UnauthorizedException;
use Lmn\Account\Lib\Auth\SigninException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Token;

class LmnAuthService implements AuthMethod{

    const KEY = "lmn-auth";

    private $config;
    private $authService;
    private $userService;
    private $validationService;

    public function __construct(){
        $this->authService = \App::make(AuthService::class);
        $this->userService = \App::make(UserService::class);
        $this->validationService = \App::make(ValidationService::class);

        $this->config = Config::get("lmn.account.access.lmn", false);
    }

    /**
     * Create new authentification record for that user. User has to be registered before signing in.
     * @method register
     * @param  array   $data auth data
     * @return Model\Auth
     */
    public function register($data){
        $auth = $this->authService->register([
            'user_id' => $data['user_id'],
            'method' => self::KEY,
            'uid' => $data['sub']
        ]);

        return $auth;
    }

    /**
     * Determine is authentification data should be handled by this service. True if method section of $requestData equals to this.KEY.
     * @method isMyClient
     * @param  array     $requestData data part of request
     * @param  Model\User     $currUser
     * @return boolean true if 'method' section of $requestData equals to self::KEY.
     */
    public function isMyClient($requestData, $currUser = null){
        if (!isset($requestData['method'])){
            return false;
        }

        return ($requestData['method'] == self::KEY);
    }

    /**
     * Verify data with our user database. Check email and password combination
     * @method verify
     * @param  array $auth array that contains email and password values
     * @throws AuthorizationFailedException
     * @return GoogleUser
     */
    public function verify($auth){
        if (!$this->validationService->systemValidate($auth, 'auth.lmn')) {
            return null;
        }
        $user = $this->userService->check($auth['email'], $auth['password']);
        return $user;
    }

    /**
     * Sign in user with authentification data.
     * @method signin
     * @param  array $authData auth part of request data
     * @throws SigninException
     * @return Model\User
     */
    public function signin($authData){
        $this->validationService->validateOrFail($authData, 'auth.lmn');
        $verifyUser = $this->verify($authData);

        if ($verifyUser == null) {
            throw new SigninException("Authorization failed.");
        }

        return $verifyUser;
    }

    /**
     * Sign out is handled by AuthService signout method.
     * @method signout
     */
    public function signout(){

    }
}

<?php

namespace Lmn\Account\Lib\Google;

use Lmn\Core\Lib\Facade\Config;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Account\Lib\Auth\AuthMethod;
use Lmn\Account\Lib\Auth\AuthService;
use Lmn\Account\Lib\Auth\SigninException;
use Lmn\Account\Lib\User\UserService;
use Lmn\Account\Lib\Auth\InvalidAuthDataException;
use Lmn\Account\Lib\Auth\AuthorizationFailedException;
use Lmn\Account\Lib\Lmn\LmnAuthService;
use Lmn\Account\Database\Model\User;
use Google_Client;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Token;

class GoogleAuthService implements AuthMethod{

    const KEY = "google-oauth2";

    private $client;
    private $config;
    private $authService;
    private $userService;
    private $validationService;

    public function __construct(){
        $this->authService = \App::make(AuthService::class);
        $this->userService = \App::make(UserService::class);
        $this->validationService = \App::make(ValidationService::class);

        $this->config = Config::get("lmn.account.access.google", false);
        $this->client = new Google_Client();
        $this->client->setClientId($this->config['client_id']);
        $this->client->setClientSecret($this->config['client_secret']);
    }

    /**
     * Register new user and create new authentification record for that user.
     * @method register
     * @param  array   $data user and auth data
     * @return Model\Auth
     */
    public function register($data){
        $auth = $this->authService->register([
            'user_id' => $data['user_id'],
            'method' => self::KEY,
            'uid' => $data['sub']
        ]);

        return $auth;
    }

    /**
     * Determine is authentification data should be handled by this service. True if method section of $requestData equals to this.KEY.
     * @method isMyClient
     * @param  array     $requestData data part of request
     * @param  Model\User     $currUser
     * @return boolean true if 'method' section of $requestData equals to self::KEY.
     */
    public function isMyClient($requestData, $currUser = null){
        if (!isset($requestData['method'])){
            return false;
        }

        return ($requestData['method'] == self::KEY);
    }

    /**
     * Verify data with google server.
     * @method verify
     * @param  string $id_token user id_token from google.
     * @throws AuthorizationFailedException
     * @return GoogleUser
     */
    public function verify($id_token){
        $userNode = null;
        try {
            $userNode =  $this->client->verifyIdToken($id_token, $this->config['client_id'])->getAttributes();
        }
        catch (\Exception $ex) {
            throw new AuthorizationFailedException("User has not been verified with google.");
        }

        if ($userNode == null) {
            throw new AuthorizationFailedException("User has not been verified with google.");
        }
        return $userNode;
    }

    /**
     * Sign in user with authentification data.
     * @method signin
     * @param  array $authData auth part of request data
     * @throws SigninException
     * @return Model\User
     */
    public function signin($authData){
        try {
            $this->validationService->systemValidate($authData, 'auth.google');

            $googleData = $this->verify($authData['id_token']);
            $payload = $googleData['payload'];

            $user = $this->authService->authorize(self::KEY, $payload['sub']);
            if ($user == null) {
                //TODO transaction
                $user = $this->userService->register([
                    'name' => $payload['given_name'],
                    'surname' => $payload['family_name'],
                    'fullname' => $payload['name'],
                    'email' => $payload['email'],
                    'valid' => $payload['email_verified']
                ]);

                $auth = $this->register([
                    'sub' => $payload['sub'],
                    'user_id' => $user->id
                ]);
            }

            return $user;
        }
        catch (\Esception $ex) {
            throw new SigninException("Sign in failed.", $ex);
        }
    }

    /**
     * Sign out is handled by AuthService signout method.
     * @method signout
     */
    public function signout(){

    }

}

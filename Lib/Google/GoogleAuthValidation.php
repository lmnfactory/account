<?php

namespace Lmn\Account\Lib\Google;

use Lmn\Core\Lib\Model\LaravelValidation;

class GoogleAuthValidation extends LaravelValidation {

    public function __construct() {

    }

    public function getRules($data) {
        return [
            'id_token' => 'required'
        ];
    }
}

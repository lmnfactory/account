<?php

namespace Lmn\Account\Lib\User;

use Lmn\Account\Repository\UserRepository;
use Lmn\Account\Repository\UserPasswordRepository;

use Lmn\Account\Repository\Criteria\User\UserByLoginCriteria;
use Lmn\Account\Repository\Criteria\User\UserByPrivateIdCriteria;
use Lmn\Account\Repository\Criteria\User\UserDefaultCriteria;
use Lmn\Account\Repository\Criteria\User\UserValidateCriteria;

use Lmn\Core\Lib\Database\Save\SaveService;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Model\ValidationException;
use Lmn\Core\Exception\SystemException;

class UserService {

    /** @var ValidationService */
    private $validationService;
    /** @var UserRepository */
    private $userRepo;
    private $userPasswordRepo;

    public function __construct(ValidationService $validationService, UserRepository $userRepo, UserPasswordRepository $userPasswordRepo){
        $this->validationService = $validationService;
        $this->userRepo = $userRepo;
        $this->userPasswordRepo = $userPasswordRepo;
    }

    /**
     * Retrieve user by his private id
     * @method getByPrivateId
     * @param  string         $privateId user private id
     * @return Model\User
     */
    public function getByPrivateId($privateId) {
        return $this->userRepo->clear()
            ->criteria('user.by.privateId', ['privateId' => $privateId])
            ->criteria('user.default')
            ->get();
    }

    /**
     * Check if the email and password combination is correct. If it is correct return user, otherwise throw ValidationException exception
     * @method check
     * @param  string $email
     * @param  string $password
     * @throws ValidationException
     * @return Model\User
     */
    public function check($email, $password) {
        $data = [
            'email' => $email,
            'password' => $password,
        ];

        if (!$this->validationService->validate($data, 'form.signin')) {
            return null;
        }

        return $this->userRepo->clear()
            ->criteria('user.by.login', ['email' => $email])
            ->criteria('user.default')
            ->get();
    }

    /**
     * Create new user in database.
     * @method register
     * @param  array   $userData user date
     * @return Model\User
     */
    public function register($userData){
        return $this->userRepo->clear()
            ->create($userData);
    }

    /**
     * Save user in database. It can be new user or you can update exsisting one.
     * @method save
     * @param  Model\User $user
     * @throws SystemException
     * @return Model\User
     */
    public function save($user, $saveData = null) {
        //$this->userCrud->save($user, $saveData);
    }

    /**
     * Try to verify user email address. Method check if email and validationtoken are correct. If they are correct, then user email address will be mark as valid.
     * @method validate
     * @param  string   $email
     * @param  string   $token
     * @throws ValidationException
     * @return Model\User
     */
    public function validate($email, $token) {
        $data = [
            'email' => $email,
            'validationtoken' => $token
        ];
        if (!$this->validationService->validate($data, 'user.validate')) {
            return null;
        }

        $users = $this->userRepo->clear()
            ->criteria('user.validate', ['email' => $email, 'token' => $token])
            ->update([
                'valid' => true,
                'validationtoken' => null
            ]);

        return $this->userRepo->clear()
            ->criteria('user.by.email', ['email' => $email])
            ->get();
    }

    public function updateLastSignin($user) {
        $this->userRepo->clear()
            ->criteria('core.id', ['id' => $user->id])
            ->update([
                'lastsignin' => date("Y-n-j H:i:s", time())
            ]);
    }

    public function resetPassword($email, $password) {
        $this->userPasswordRepo->clear()
            ->criteria('user.by.email', ['email' => $email])
            ->update([
                'password' => $password
            ]);

        return $this->userRepo->clear()
            ->criteria('user.by.email', ['email' => $email])
            ->get();
    }
}

<?php

namespace Lmn\Account\Lib\User;

interface IUser {
    public function getId();
    public function getPublicId();
    public function getFullname();
    public function getUser();
}

<?php

namespace Lmn\Account\Lib\Test;

class TestAuth {

    private $jwtAuth;
    private $jwtRefresh;
    private $rmAuth;

    public function __construct($jwtAuth, $jwtRefresh, $rmAuth = null) {
        $this->jwtAuth = $jwtAuth;
        $this->jwtRefresh = $jwtRefresh;
        $this->rmAuth = $rmAuth;
    }

    public function getJwtAuth() {
        return $this->jwtAuth;
    }

    public function getJwtRefresh() {
        return $this->jwtRefresh;
    }

    public function getRmAuth() {
        return $this->rmAuth;
    }

    public function getRmAuthToken() {
        if ($this->rmAuth == null || !isset($this->rmAuth['series']) || !isset($this->rmAuth['token'])) {
            return "";
        }
        return $this->rmAuth['series'].".".$this->rmAuth['token'];
    }
}

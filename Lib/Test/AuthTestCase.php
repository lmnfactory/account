<?php

namespace Lmn\Account\Lib\Test;

use Lmn\Account\Lib\Test\TestAuth;
use TestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

abstract class AuthTestCase extends TestCase {

    private $authResponse = null;

    protected function signin() {
        $response = $this->call('POST', '/auth/signin', [], [], [], [], json_encode([
                'auth' => [
                    'email' => 'local-1@mail.abc',
                    'password' => 'aaaaaa'
                ],
                'method' => 'lmn-auth',
                'rememberMe' => true
            ]));

        $responseData = $response->getData(true);

        $rmAuth = null;
        if (isset($responseData['header']['RMAuth'])) {
            $rmAuth = $responseData['header']['RMAuth'];
        }

        return new TestAuth($responseData['header']['JWTAuth'], $responseData['header']['JWTRefresh'], $rmAuth);
    }

    protected function authJson($method, $uri, $data, $headers = []) {
        if ($this->authResponse == null) {
            $this->authResponse = $this->signin();
        }

        return $this->json($method, $uri, $data, [
            'HTTP_JWTAuth' => $this->authResponse->getJwtAuth()
        ]);
    }
}

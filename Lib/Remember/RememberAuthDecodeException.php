<?php

namespace Lmn\Account\Lib\Remember;

class RememberAuthDecodeException extends \Exception {

    public function __construct($message, $code = null, $previous = null){
        parent::__construct($message, $code, $previous);
    }
}

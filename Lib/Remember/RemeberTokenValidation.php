<?php

namespace Lmn\Account\Lib\Remember;

use Lmn\Core\Lib\Model\LaravelValidation;

class RememberTokenValidation extends LaravelValidation {

    public function __construct() {

    }

    public function getRules($data) {
        return [
            'series' => 'required',
            'token' => 'required'
        ];
    }
}

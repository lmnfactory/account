<?php

namespace Lmn\Account\Lib\Remember;

use Illuminate\Http\Request;
use Lmn\Account\Lib\Remember\RememberModelToken;
use Lmn\Account\Lib\Remember\RMAuthToken;
use Lmn\Account\Lib\Remember\Token\RememberToken;
use Lmn\Account\Lib\User\UserService;
use Lmn\Account\Lib\Auth\Token\TokenService;

use Lmn\Account\Database\Model\Remember;
use Lmn\Account\Database\Model\User;
use Lmn\Account\Repository\UserRepository;
use Lmn\Account\Repository\RememberRepository;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Database\GeneratorService;

class RememberService {

    private $token;
    private $userRepo;
    private $rememberRepo;
    private $generatorService;
    private $expire;
    private $changed;

    public function __construct(TokenService $tokenService, UserRepository $userRepo, RememberRepository $rememberRepo, GeneratorService $generatorService) {
        $this->tokenService = $tokenService;
        $this->userRepo = $userRepo;
        $this->rememberRepo = $rememberRepo;
        $this->generatorService = $generatorService;
        $this->token = null;
        $this->setExpireTime(14 * 24 * 60 * 60);
        $this->changed = false;
    }

    private function init() {
        if ($this->token == null) {
            $this->token = $this->tokenService->create('remember');
        }
    }

    private function setTokenByRemember($remember) {
        $this->init();
        $this->token->set('series', $remember->series)
            ->set('remember', $remember)
            ->set('expires_at', $remember->expires_at);
    }

    private function setTokenByData($data) {
        $this->init();
        foreach ($data as $key => $val) {
            $this->token->set($key, $val);
        }
    }

    /**
     * Extract Remember me token from request header.
     * @method decode
     * @param  Request $request
     * @throws RememberAuthDecodeException
     * @return RememberToken
     */
    private function decode($RMAuthHeader) {
        $this->init();

        $this->token->decode($RMAuthHeader);

        $remembers = $this->rememberRepo->clear()
            ->criteria('remember.by.series', ['series' => $this->token->get('series')])
            ->all();
        foreach ($remembers as $r) {
            if (password_verify($this->token->get('token'), $r->token)) {
                $this->setTokenByRemember($r);
                break;
            }
        }
    }

    private function changeUserSeries() {
        $this->userRepo->clear()
            ->criteria('user.by.series', ['series' => $this->token->get('series')])
            ->update([
                'series' => $this->generatorService->uniqueString('user', 'series')
            ]);
    }

    private function refreshToken() {
        $tokenStr = str_random(32);
        $data = [
            'token' => password_hash($tokenStr, PASSWORD_DEFAULT),
            'expires_at' => time() + $this->getExpireTime()
        ];

        $remember = $this->token->get('remember');
        $this->rememberRepo->clear()
            ->criteria('core.id', ['id' => $remember->id])
            ->update($data);
        $remember = $this->rememberRepo->clear()
            ->criteria('core.id', ['id' => $remember->id])
            ->get();

        $this->setTokenByData(['token' => $tokenStr]);
        $this->setTokenByRemember($remember);
        $this->changed = true;
    }

    private function deleteSeries() {
        $this->rememberRepo->clear()
            ->criteria('remember.by.series', ['series' => $this->token->get('series')])
            ->delete();
    }

    private function getUser($userId) {
        return $this->userRepo->clear()
            ->criteria('core.id', ['id' => $userId])
            ->get();
    }

    private function matchSeries($token) {
        return $this->rememberRepo->clear()
            ->criteria('remember.by.series', ['series' => $token->get('series')])
            ->get();
    }

    /**
     * Set value for expiration time for remember me token. This value will be added to current time.
     * @method setExpireTime
     * @param  integer        $expireIn number of seconds
     */
    public function setExpireTime($expireIn) {
        $this->expire = $expireIn;
    }

    /**
     * Retrieve expiration time. This time is not added to current time.
     * @method getExpireTime
     * @return integer number of seconds
     */
    public function getExpireTime() {
        return $this->expire;
    }

    /**
     * Check, if current remember me token has changed.
     * @method hasChanged
     * @return boolean
     */
    public function hasChanged() {
        return $this->changed;
    }

    /**
     * Retrieve current remember me token.
     * @method getToken
     * @return RememberToken
     */
    public function getToken() {
        return $this->token;
    }

    /**
     * Disable current remember me token.
     * @method signout
     */
    public function signout($tokenStr = "") {
        // TODO: remove try catch
        try {
            $this->decode($tokenStr);
            if ($this->token->isValid()){
                $remember =$this->token->get('remember');
                $remember->delete();
            }
        }
        catch (\Exception $ex) {

        }
    }

    /**
     * Create remember me token for $user
     * @method remember
     * @param  Model\User   $user
     * @return Model\Remember
     */
    public function remember($user) {
        $token = str_random(32);
        $data = [
            'user_id' => $user->id,
            'series' => $user->series,
            'token' => password_hash($token, PASSWORD_DEFAULT),
            'expires_at' => time() + $this->getExpireTime()
        ];

        $remember = $this->rememberRepo->clear()
            ->create($data);
        $this->setTokenByData(['token' => $token]);
        $this->setTokenByRemember($remember);
        $this->changed = true;
    }

    /**
     * Try to sign in with remember me token. If token is not valid, than assume it's fraud and remove all tokens with that series. If it is a valid token then return user for that token and create new token hash.
     * @method signin
     * @return Model\User or null if nobody was authorized with that remember me token
     */
    public function signin($token) {
        $this->decode($token);
        if ($this->matchSeries($this->token)) {
            if (!$this->token->has('remember')) {
                $this->deleteSeries();
                $this->changeUserSeries();
            }
            else if ($this->token->isValid()){
                $this->refreshToken();
                $remember = $this->token->get('remember');
                return $this->getUser($remember->user_id);
            }
        }
        return null;
    }
}

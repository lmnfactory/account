<?php

namespace Lmn\Account\Lib\Remember\Token;

use Lmn\Account\Lib\Auth\Token\Token;

class RememberToken extends Token {

    private $tokenData;

    public function __construct() {
        parent::__construct();
    }

    public function isValid() {
        return ($this->has('expires_at') && $this->get('expires_at') > time());
    }

    public function encode() {
        return [
            'series' => $this->get('series'),
            'token' => $this->get('token')
        ];
    }

    public function decode($hash) {
        $exp = explode(".", $hash);
        if (sizeof($exp) == 2) {
            $this->set('series', $exp[0]);
            $this->set('token', $exp[1]);
        }
        else {
            $this->set('series', "");
            $this->set('token', "");
        }
    }
}
